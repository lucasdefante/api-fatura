package br.com.itau.fatura.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new CartaoNotFoundException();
        }
        if(response.status() == 400) {
            return new CartaoBadRequestException();
        }
        return errorDecoder.decode(s, response);
    }
}
