package br.com.itau.fatura.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PagamentoClientConfiguration {

    @Bean
    public ErrorDecoder getPagamentoClientDecoder() {
        return new br.com.itau.fatura.clients.PagamentoClientDecoder();
    }
}
