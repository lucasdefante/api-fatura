package br.com.itau.fatura.clients;


import br.com.itau.fatura.DTOs.ConsultarCartaoDTO;
import br.com.itau.fatura.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(name = "CARTAO", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/id/{id}")
    Cartao getCartaoById(@PathVariable int id);

    @PatchMapping("/cartao/{numero}")
    ConsultarCartaoDTO ativarOuDesativar(@RequestBody Map<String, Boolean> map, @PathVariable String numero);
}
