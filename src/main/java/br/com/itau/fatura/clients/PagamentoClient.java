package br.com.itau.fatura.clients;


import br.com.itau.fatura.DTOs.ListaPagamentoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name = "PAGAMENTO", configuration = PagamentoClientConfiguration.class)
public interface PagamentoClient {

    @GetMapping("/pagamentos/{cartaoId}")
    List<ListaPagamentoDTO> consultarPagamentos(@PathVariable int cartaoId);

    @PostMapping("/deletar/{cartaoId}")
    void deletarPagamentos(@PathVariable int cartaoId);
}
