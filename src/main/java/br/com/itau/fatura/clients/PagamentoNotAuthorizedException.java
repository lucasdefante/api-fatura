package br.com.itau.fatura.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Pagamento não autorizado. Cartão se encontra inativo")
public class PagamentoNotAuthorizedException extends RuntimeException {
}
