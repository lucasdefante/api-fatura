package br.com.itau.fatura.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration {

    @Bean
    public ErrorDecoder getCartaoClientDecoder() {
        return new br.com.itau.fatura.clients.CartaoClientDecoder();
    }
}
