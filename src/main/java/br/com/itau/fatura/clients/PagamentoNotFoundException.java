package br.com.itau.fatura.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Não existem pagamentos a serem faturados para o cartão")
public class PagamentoNotFoundException extends RuntimeException {
}
