package br.com.itau.fatura.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Corpo da requisição não contém o campo esperado")
public class CartaoBadRequestException extends RuntimeException {
}
