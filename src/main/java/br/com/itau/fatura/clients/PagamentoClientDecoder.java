package br.com.itau.fatura.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class PagamentoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            throw new PagamentoNotFoundException();
        }
        if(response.status() == 400) {
            throw new PagamentoNotAuthorizedException();
        }
        return errorDecoder.decode(s, response);
    }
}
