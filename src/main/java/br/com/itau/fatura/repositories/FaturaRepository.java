package br.com.itau.fatura.repositories;

import br.com.itau.fatura.models.Fatura;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura, Integer> {
}
