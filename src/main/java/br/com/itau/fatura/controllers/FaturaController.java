package br.com.itau.fatura.controllers;

import br.com.itau.fatura.DTOs.ExpirarCartaoDTO;
import br.com.itau.fatura.DTOs.PagarFaturaDTO;
import br.com.itau.fatura.clients.CartaoClient;
import br.com.itau.fatura.clients.PagamentoClient;
import br.com.itau.fatura.models.Cartao;
import br.com.itau.fatura.services.FaturaService;
import br.com.itau.fatura.DTOs.ListaPagamentoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @Autowired
    private CartaoClient cartaoClient;

    @Autowired
    private PagamentoClient pagamentoClient;

    @GetMapping("/{clienteId}/{cartaoId}")
    public List<ListaPagamentoDTO> exibirFatura(@PathVariable(name = "clienteId") int clienteId,
                                                @PathVariable(name = "cartaoId") int cartaoId) {
        Cartao cartao = cartaoClient.getCartaoById(cartaoId);
        if (cartao.getClienteId() != clienteId) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartão consultado não pertence ao cliente especificado");
        }
        return pagamentoClient.consultarPagamentos(cartao.getId());
    }

    @PostMapping("/{clienteId}/{cartaoId}/pagar")
    public PagarFaturaDTO pagarFatura(@PathVariable(name = "clienteId") int clienteId,
                                      @PathVariable(name = "cartaoId") int cartaoId) {
        List<ListaPagamentoDTO> listaPagamentoDTOS = exibirFatura(clienteId, cartaoId);
        PagarFaturaDTO pagarFaturaDTO = new PagarFaturaDTO();
        Cartao cartao = cartaoClient.getCartaoById(cartaoId);
        pagarFaturaDTO = faturaService.pagarFatura(cartao, listaPagamentoDTOS);
        pagamentoClient.deletarPagamentos(cartao.getId());
        return pagarFaturaDTO;
    }

    @PostMapping("/{clienteId}/{cartaoId}/expirar")
    public ExpirarCartaoDTO expirarCartao(@PathVariable(name = "clienteId") int clienteId,
                                          @PathVariable(name = "cartaoId") int cartaoId) {
        Cartao cartao = cartaoClient.getCartaoById(cartaoId);
        if (cartao.getClienteId() != clienteId) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartão consultado não pertence ao cliente especificado");
        }
        Map<String, Boolean> map = faturaService.expirarCartao(cartao);
        cartaoClient.ativarOuDesativar(map, cartao.getNumero());
        return new ExpirarCartaoDTO("ok");
    }
}
