package br.com.itau.fatura.services;

import br.com.itau.fatura.models.Cartao;
import br.com.itau.fatura.DTOs.ExpirarCartaoDTO;
import br.com.itau.fatura.DTOs.PagarFaturaDTO;
import br.com.itau.fatura.models.Fatura;
import br.com.itau.fatura.repositories.FaturaRepository;
import br.com.itau.fatura.DTOs.ListaPagamentoDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FaturaService {

    @Autowired
    private FaturaRepository faturaRepository;

    public PagarFaturaDTO pagarFatura(Cartao cartao, List<ListaPagamentoDTO> listaPagamentoDTOS) {
        double valorPago = 0;
        LocalDate pagoEm = LocalDate.now();
        for (ListaPagamentoDTO pagamento:
             listaPagamentoDTOS) {
            valorPago += pagamento.getValor();
        }
        Fatura fatura = new Fatura();
        fatura.setCartaoId(cartao.getId());
        fatura.setPagoEm(pagoEm);
        fatura.setValorPago((new BigDecimal(valorPago).setScale(2, RoundingMode.DOWN)).doubleValue());
        Fatura faturaDB = faturaRepository.save(fatura);
        return new PagarFaturaDTO(faturaDB.getId(), faturaDB.getValorPago(), faturaDB.getPagoEm());
    }

    public Map<String, Boolean> expirarCartao(Cartao cartao) {
        Map<String, Boolean> map = new HashMap<>();
        map.put("ativo", false);
        return map;
    }
}
